# Hybrid-CNN-ViT
Author: Duncan Goudie

Alexey Dosovitskiy was the first to introduce Vision Transformers (ViT) to the world; ViTs were inspired from Transformers in the NLP world and directly applied to imaging tasks.

In this project, I built an image classification hybrid ViT, which pre-stacks a pretrained CNN backbone onto a ViT. Without a CNN backbone, a ViT uses flattened image patches directly as the input tokens. With a CNN backbone, the image patches are processed into a feature map before being used as the input token.

I saved myself time and used the pretrained models and classes for Resnet18 (my CNN backbone) and ViT from PyTorch's Torchvision model library. To make them compatible with each other, I created a new ViT class and overrode some of the internal functions within it; this included changing how the image patches were flattened into token vectors. For the input tokens, I took the third layer feature map from Resnet18 because I noticed that the height and width was 14x14, exactly the number of tokens the pretrained ViT expects. Each depth-wise column of the feature map was taken as the input to the ViT and processed into a token of the correct length via a modified `conv_proj` layer.

I was interested to understand Transformers on an implementation level, and given my computer vision background, thought this would be a nice and quick educational project!

## Docker

Please build the docker image in [folder name] using the following command. The image size will likely come to around 19GB.
```
cd docker
./build.sh
```

You can alternatievly pull it from my dockerhub with,
```
docker pull duncangoudie/pytorch_cv:1.13.1-cuda11.6-cudnn8
```

Please pull an interactive container by running the following docker command (make the appropriate changes to your folder paths),

```
docker run -it --rm --gpus all -v /path/to/repo/:/Development -v /path/to/dataset/:/dataset_folder -v /path/to/model/save/folder/:/save_folder/ --workdir /Development  duncangoudie/pytorch_cv:1.13.1-cuda11.6-cudnn8 $SHELL
```


## Using it

### Dataset

I used a subset from the Flickr images that were listed under Imagenet. You can download the exact training and testing split here (you will have to contact me first to be granted permission).

It comprises of 12 classes, with a bias towards animals, but with a few inanimate objects in.

You can download this dataset from the following link: https://drive.google.com/file/d/1pR_d0ftUFc0zPI6NhNTmTIWgCklKS13e/view?usp=sharing

It has the following folder structure,
```
|- train/
|-- class1/
|-- class2/
|-- ...
|-- classX/

|- test/
|-- ...

|- val/
|-- ...
```

### Training

You can train your own Hybrid-CNN-ViT by first modifying `hybrid_cnn_vit_classifier/configs/config_test.yml` to suit your setup. Then going into `hybrid_cnn_vit_classifier/` and running,
```
python3 train.py --config configs/config_train.yml
```

### Testing

You can test with either my model or with your own trained Hybrid-CNN-ViT.

First you need to modify `hybrid_cnn_vit_classifier/configs/config_test.yml` to match with your model file and csv training list locations.

Then you can run it in `hybrid_cnn_vit_classifier/` with,

```
python3 test.py --config configs/config_test.yml
```

### Troubleshooting

If you are having trouble running this, perhaps the following suggestions can resolve it.

- Received the following error which failed to download resnet18 from pytorch
```
Downloading: "https://download.pytorch.org/models/resnet18-f37072fd.pth" to /root/.cache/torch/hub/checkpoints/resnet18-f37072fd.pth
...
urllib.error.URLError: <urlopen error [Errno -2] Name or service not known>
```
It is possible we were not able to access PyTorch's web storage; I have found on some occasions the PyTorch model downloads work and other times it does not. Try using the following docker image: `duncangoudie/pytorch_cv:1.13.1-cuda11.6-cudnn8_hybrid_cnn_vit`. This one has resnet18 preloaded into it.



### Results

You can download a Hybrid-CNN-ViT I trained from the following link: https://drive.google.com/file/d/1TBIRWpbsxjDHIGo565JevuxqrkLc7odd/view?usp=share_link

Hybrid-CNN-ViT: top-1 accuracy: 63%

Resnet-18: [TODO: Perform test to provide benchmark]


## References

```
@article{dosovitskiy2020vit,
  title={An Image is Worth 16x16 Words: Transformers for Image Recognition at Scale},
  author={Dosovitskiy, Alexey and Beyer, Lucas and Kolesnikov, Alexander and Weissenborn, Dirk and Zhai, Xiaohua and Unterthiner, Thomas and  Dehghani, Mostafa and Minderer, Matthias and Heigold, Georg and Gelly, Sylvain and Uszkoreit, Jakob and Houlsby, Neil},
  journal={ICLR},
  year={2021}
}
```