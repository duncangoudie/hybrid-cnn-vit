#! /bin/bash

DOCKERFILE="Dockerfile-duncangoudie-pytorch_cv-1.13.1-cuda11.6-cudnn8"
IMAGENAME="duncangoudie/pytorch_cv:1.13.1-cuda11.6-cudnn8"

docker build -f ${DOCKERFILE} -t ${IMAGENAME} .