"""
Data loader for Hybrid CNN-ViT Classifier

"""


import torch
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import numpy as np
import cv2
from PIL import Image
import os
import pandas as pd
import matplotlib.pyplot as plt

class ClassificationDataLoader:

    def __init__(self):
        self.image_size = 224

    def get_dataloader(self,
                      root_folder_path: str,
                      data_csv_filepath: str,
                      train:bool=False,
                      batch_size=16):


        # Select the online augmentations to perform for training or testing
        if train==True:
            # Design our online data augmentations for training
            compose_transforms = transforms.Compose([transforms.RandomRotation(degrees=10),
                                                           transforms.RandomCrop(size=self.image_size,pad_if_needed=True),
                                                           transforms.RandomHorizontalFlip(p=0.5),
                                                           transforms.TrivialAugmentWide(),
                                                           transforms.ToTensor(),
                                                           transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))])
        else:
            # Design our input data preprocessing for test time
            compose_transforms = transforms.Compose([transforms.Resize((self.image_size, self.image_size)),
                                                          transforms.ToTensor(), # ToTensor converts a PIL image from 0-255 uint8 to 0-1 float
                                                          transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))])

        # Create instances of our ClassificationDataset class.
        # We create our own Dataset class because we our read and open datapoint files is custom
        dataset = self.ClassificationDataset(root_folder_path=root_folder_path,
                                                   file_list_csv_filepath=data_csv_filepath,
                                                   transform=compose_transforms)



        # Create our torch DataLoader
        if train == True:
            dataloader = torch.utils.data.DataLoader(dataset,
                                                     batch_size=batch_size,
                                                     shuffle=True,
                                                     num_workers=4)
        else:
            dataloader = torch.utils.data.DataLoader(dataset,
                                                     batch_size=batch_size,
                                                     shuffle=False,
                                                     num_workers=1)

        dataset_size = len(dataset)

        return dataloader, dataset_size

    class ClassificationDataset(Dataset):
        """
        Our own classification dataset using the folder structure
        -train/
        |-class1/
        |-class2/
        ...
        -val/
        |-class1/
        |-class2/
        ...
        -test/
        |-class1/
        |-class2/

        We read a csv file containing these structures
        """

        def __init__(self, root_folder_path: str, file_list_csv_filepath: str, transform=None):
            """

            :param root_folder_path:
            :param file_list_csv:
            """

            self._root_folder_path = root_folder_path
            self._df_file_list = pd.read_csv(file_list_csv_filepath)
            self.transform = transform


        def __len__(self):
            return len(self._df_file_list)

        def __getitem__(self, idx):
            if torch.is_tensor(idx):
                idx = idx.tolist()

            # load image and label
            relative_img_file_path = self._df_file_list['filepath'].iloc[idx]
            img_full_path = os.path.join(self._root_folder_path, relative_img_file_path)
            img = Image.open(img_full_path)
            label = int(self._df_file_list['label_id'].iloc[idx])

            # Perform online augmentations (if defined)
            if self.transform:
                img = self.transform(img)

            return img, label


