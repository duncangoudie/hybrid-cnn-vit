"""

Hybrid CNN-ViT is a Vision Transformer model with ResNet18 attached to the front.

Usually, the ViT splits a 224x224 image into a 14x14 patch grid of 16x16 size patches, where each patch is then flattened into a token.
The trick we use is to take the activation map output after the 3rd layer of ResNet18, which is size 256x14x14 (CxHxW)
and use each "depthwise column" as a 256x1x1 sized token. The beauty is that we already have 14x14 tokens for the ViT!

The expected token size for ViT_B_16 is 768. Normally, the ViT_B_16 pytorch implementation uses an input convolutional
layer (conv_proj) to transform a 3x224x224 image into 768x14x14. So with our ResNet18 activation map input, we replace
the conv_proj convolutional layer with one that transforms 256x14x14 into 768x14x14.

In summary:
 - Use PyTorch's ViT_B_16 pretrained model as the ViT
 - ViT expects 14x14 patches at its input
 - the activation map after ResNet18's third layer is size 256x14x14 (CxHxW)
 - replace conv_proj with a new convolutional layer that transforms (convolves) 256x14x14 into 768x14x14.


"""

import numpy as np
import cv2
from PIL import Image
import matplotlib.pyplot as plt
from tqdm import tqdm, trange

import torch
import torch.nn as nn
from torch.optim import Adam
from torch.nn import CrossEntropyLoss
from torch.utils.data import DataLoader

import torchvision
from torchvision.transforms import ToTensor
from torchvision import transforms
from torchvision.models import resnet18
from torchvision.models import ResNet18_Weights

from torchvision.models.vision_transformer import VisionTransformer
from torchvision.models import vision_transformer


class AlteredVisionTransformer(VisionTransformer):
    """
    Override the forward and _process_input functions so that the pytorch implemented ViT does not require processing on conv_proj.
    We will place responsibility for making sure the input tensor to ViT matches up outside of the ViT.
    """

    def _process_input(self, x: torch.Tensor) -> torch.Tensor:
        n, c, h, w = x.shape
        p = self.patch_size
        #torch._assert(h == self.image_size, f"Wrong image height! Expected {self.image_size} but got {h}!")
        #torch._assert(w == self.image_size, f"Wrong image width! Expected {self.image_size} but got {w}!")
        n_h = h // p
        n_w = w // p

        # (n, c, h, w) -> (n, hidden_dim, n_h, n_w)
        x = self.conv_proj(x)
        # (n, hidden_dim, n_h, n_w) -> (n, hidden_dim, (n_h * n_w))
        #x = x.reshape(n, self.hidden_dim, n_h * n_w) # We don't need this line anymore as our activation map is already 14x14 (correct dimensions)
        # Instead, just do a flatten on the height x width dimensions
        x = x.reshape(n, self.hidden_dim, h * w)

        # (n, hidden_dim, (n_h * n_w)) -> (n, (n_h * n_w), hidden_dim)
        # The self attention layer expects inputs in the format (N, S, E)
        # where S is the source sequence length, N is the batch size, E is the
        # embedding dimension
        x = x.permute(0, 2, 1)

        return x

    def forward(self, x: torch.Tensor):
        # Reshape and permute the input tensor
        x = self._process_input(x)
        n = x.shape[0]

        # Expand the class token to the full batch
        batch_class_token = self.class_token.expand(n, -1, -1)
        x = torch.cat([batch_class_token, x], dim=1)

        x = self.encoder(x)

        # Classifier "token" as used by standard language architectures
        x = x[:, 0]

        x = self.heads(x)

        return x

class HybridCnnVit(nn.Module):
    """
    This works for pretrained ViT model ViT_B_16
    """

    def __init__(self, no_classes: int, load_pretrained: bool=False):
        """

        :param no_classes:
        :param load_pretrained: set to True if you want to load pretrained ViT_B_16 weights
        """
        super().__init__()

        self._image_size = 224

        # Load up our CNN Backbone, resnet18
        resnet = resnet18(ResNet18_Weights.IMAGENET1K_V1)
        for p in resnet.parameters():
            p.requires_grad = False
        # We will only use the first 3 layers of it, so that the activation map is of shape 256x14x14, instead of 512x7x7 for the fourth and final layer.
        self.cnn_backbone = nn.Sequential( resnet.conv1,
                                           resnet.bn1,
                                           resnet.relu,
                                           resnet.maxpool,
                                           resnet.layer1,
                                           resnet.layer2,
                                           resnet.layer3)

        # Instantiate our modified ViT
        self.avit = AlteredVisionTransformer( image_size=self._image_size,
                                              patch_size=16,
                                              num_layers=12,
                                              num_heads=12,
                                              hidden_dim=768,
                                              mlp_dim=3072)
        # worth noting that we set patch_size=16 just to respect the original pre-trained weight structure.
        # we intend to replace the conv_proj layer completely and accept patch sizes of 1 instead.
        # as a note, pytorch's ViT uses conv_proj as a neat trick for tokenising an image

        # Load pretrained weights if desired (requirement for training from "scratch")
        if load_pretrained == True:
            vit_weights = vision_transformer.ViT_B_16_Weights.IMAGENET1K_V1.get_state_dict(progress=True)
            self.avit.load_state_dict(vit_weights)

        # modify conv_proj within avit to accept activation maps of size 256x14x14 instead of images of size 3x224x224
        # this will accept 14x14 patch sizes of 1 from the activation map because the original ViT accepted 14 patches of size 16x16 from images of size 224x224
        # the token lengths accepted by ViT_B_16 are length 768, so we need to transform each depthwise "column" in the activation map from being length 256 to become a learned embedding of length 768
        self.avit.conv_proj = nn.Conv2d(in_channels=256, out_channels=768, kernel_size=(1,1), stride=1)

        # replace the final MLP layer to cope with number of classes in our problem
        self.avit.heads.head = nn.Linear(in_features=768, out_features=no_classes, bias=True)

        # string them all together to create our final hybrid cnn-vit model
        self.hybrid_cnn_vit = nn.Sequential(self.cnn_backbone,
                                            self.avit)

    def forward(self, x: torch.Tensor):

        y = self.hybrid_cnn_vit(x)

        return y


