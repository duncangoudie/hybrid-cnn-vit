"""
Generate training, validation and testing lists

We expect the data to be already split into training, testing and validation folders. Within each folder, the categories
are separated into their own folders as well. The folder structure therefore looks like:

|- train/
|-- class1/
|-- class2/
|-- ...
|-- classX/

|- test/
|-- ...

|- val/
|-- ...

This script looks into this folder structure and generates a csv containing the relative paths of each image file and a category
label as a number. A legend of the category labels is given in another csv file, linking numbers with category strings.

Eg,

"""

import pandas as pd
import os
import re
import argparse



class TrainingAndTestingLists:

    def generate_data_list(self, data_folder):
        """
        Expect the data folder to have the following structure
        eg,
        |- train/
        |-- class1/
        |--- image1.jpg
        |--- image2.jpg
        |-- class2/
        |-- ...
        |-- classX/

        :param data_folder:
        :return:
        """

        data_list_file_list = []
        data_list_label_list = []

        label_legend_idx_list = []
        label_legend_name_list = []

        files_and_folders = os.listdir(data_folder)
        # sort them
        files_and_folders.sort(key=lambda f: int(re.sub('\D', '', f)))

        label_idx = 0
        for item_name in files_and_folders:
            # construct the full path of the item (file or folder)
            item_path = os.path.join(data_folder, item_name)

            # Only generate the data list of images if it is a folder
            if os.path.isdir(item_path):

                # add jpg and png files within folder to data_list, as well as the label
                all_file_list = os.listdir(item_path)
                image_file_list = [os.path.join(item_name, f) for f in all_file_list if "jpg" in os.path.splitext(f)[1]
                                   or "png" in os.path.splitext(f)[1]
                                   or "jpeg" in os.path.splitext(f)[1]]
                image_label_list = [label_idx for f in all_file_list if "jpg"  in os.path.splitext(f)[1]
                                    or "png"  in os.path.splitext(f)[1]
                                    or "jpeg" in os.path.splitext(f)[1]]

                # add file lists to existing list
                data_list_file_list += image_file_list
                data_list_label_list += image_label_list

                # add to legend
                label_legend_idx_list.append(label_idx)
                label_legend_name_list.append(item_name)
                label_idx += 1

        # create a pandas DataFrame
        df_data_list = pd.DataFrame({"filepath": data_list_file_list,
                                     "label_id": data_list_label_list})
        df_label_legend = pd.DataFrame({"id": label_legend_idx_list,
                                       "category": label_legend_name_list})

        return df_data_list, df_label_legend


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--data_folder",
                        help="",
                        action="store")
    parser.add_argument("--output_folder",
                        help="",
                        action="store")


    args =parser.parse_args()
    if args.data_folder is not None:

        data_folder = args.data_folder
        output_folder = args.output_folder

        tatl = TrainingAndTestingLists()
        df_data_list, df_label_legend = tatl.generate_data_list(data_folder=data_folder)

        folder_name = os.path.basename(os.path.normpath(data_folder))
        df_data_list.to_csv(os.path.join(output_folder, "{folder_name}_file_list.csv".format(folder_name=folder_name)))
        df_label_legend.to_csv(os.path.join(output_folder, "{folder_name}_label_legend.csv".format(folder_name=folder_name)),
                               index=False)

    else:
        print('Incorrect arguments...')



if __name__== "__main__":
    main()