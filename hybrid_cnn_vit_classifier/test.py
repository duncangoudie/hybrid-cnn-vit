"""
Use this training script to train your CNN-ViT classifier
"""

import numpy as np
from tqdm import tqdm, trange
import yaml

import torch
import torch.nn as nn
from torch.optim import Adam
from torch.nn import CrossEntropyLoss
from torch.utils.data import DataLoader

from torchvision.transforms import ToTensor
from torchvision.datasets.mnist import MNIST

from classification_data_loader import ClassificationDataLoader
from hybrid_cnn_vit import HybridCnnVit

import argparse

class ConfigReader:

    def __init__(self, config_filepath: str):
        """
        :param config_file:
        """

        self._config_filepath = config_filepath
        self._parameters = self._read_config_file(self._config_filepath)

        self.test_dataset_csv_filepath = self._parameters['test_dataset_csv_filepath']
        self.test_dataset_root_filepath = self._parameters['test_dataset_root_filepath']

        self.num_classes = self._parameters['num_classes']
        self.batch_size = self._parameters['batch_size']

        self.model_filepath = self._parameters['model_filepath']



    def _read_config_file(self, config_filepath):
        """
        :param config_filepath:
        :return:
        """

        with open(config_filepath, 'r') as ymlfile:
            config_contents = yaml.safe_load(ymlfile)

        parameters = {}

        parameters['test_dataset_csv_filepath'] = config_contents['test_dataset_csv_filepath']
        parameters['test_dataset_root_filepath'] = config_contents['test_dataset_root_filepath']

        parameters['model_filepath'] = config_contents['model_filepath']



        parameters['num_classes'] = config_contents['num_classes']
        parameters['batch_size'] = config_contents['batch_size']


        return parameters


class CnnVitTester:

    def __init__(self, config_reader: ConfigReader):

        self._config_reader = config_reader

        self._test_dataset_csv_filepath = self._config_reader.test_dataset_csv_filepath
        self._test_dataset_root_filepath = self._config_reader.test_dataset_root_filepath
        self._model_filepath = self._config_reader.model_filepath
        self._num_classes = self._config_reader.num_classes
        self._batch_size = self._config_reader.batch_size

        # initialise the dataloader
        classification_data_loader = ClassificationDataLoader()
        self._test_dataloader, self._test_dataloader_size = classification_data_loader.get_dataloader(root_folder_path=self._test_dataset_root_filepath,
                                                                                                      data_csv_filepath=self._test_dataset_csv_filepath,
                                                                                                      batch_size=self._batch_size,
                                                                                                      train=False)


    def test(self):


        # load model
        model = HybridCnnVit(no_classes=self._num_classes,
                               load_pretrained=False)
        model.load_state_dict(torch.load(self._model_filepath))

        # move the model over to GPU (if available)
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        print("Using device: ", device, f"({torch.cuda.get_device_name(device)})" if torch.cuda.is_available() else "")
        model = model.to(device)

        # evaluate the model by iterating through the test data
        criterion = CrossEntropyLoss()
        with torch.no_grad():
            correct = 0
            total = 0
            test_loss = 0.0
            for batch in tqdm(self._test_dataloader, desc="Testing"):
                x, y = batch
                x, y = x.to(device), y.to(device)
                y_hat = model(x)
                #print("y_hat: ", torch.argmax(y_hat, dim=1).detach().cpu())
                #print("y: ", y.detach().cpu())
                loss = criterion(y_hat, y)
                test_loss += loss.detach().cpu().item() / len(self._test_dataloader)

                correct += torch.sum(torch.argmax(y_hat, dim=1) == y).detach().cpu().item()
                total += len(x)
            print(f"Test loss: {test_loss:.2f}")
            print(f"Test accuracy: {correct / total * 100:.2f}%")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config",
                        help="",
                        action="store")



    args =parser.parse_args()
    if args.config is not None:

        config_filepath = args.config

        config_reader = ConfigReader(config_filepath=config_filepath)
        tester = CnnVitTester(config_reader=config_reader)
        tester.test()



    else:
        print('Incorrect arguments...')



if __name__== "__main__":
    main()