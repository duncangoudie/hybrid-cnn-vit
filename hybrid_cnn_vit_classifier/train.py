"""
Use this training script to train your CNN-ViT classifier
"""

import numpy as np
from tqdm import tqdm, trange
import yaml
import copy
import os

import torch
import torch.nn as nn
from torch.optim import Adam
from torch.nn import CrossEntropyLoss
from torch.utils.data import DataLoader

from torchvision.transforms import ToTensor
from torchvision.datasets.mnist import MNIST

from classification_data_loader import ClassificationDataLoader
from hybrid_cnn_vit import HybridCnnVit

import argparse

class ConfigReader:

    def __init__(self, config_filepath: str):
        """
        :param config_file:
        """

        self._config_filepath = config_filepath
        self._parameters = self._read_config_file(self._config_filepath)

        self.train_dataset_csv_filepath = self._parameters['train_dataset_csv_filepath']
        self.train_dataset_root_filepath = self._parameters['train_dataset_root_filepath']
        self.val_dataset_csv_filepath = self._parameters['val_dataset_csv_filepath']
        self.val_dataset_root_filepath = self._parameters['val_dataset_root_filepath']

        self.learning_rate = self._parameters['learning_rate']
        self.num_classes = self._parameters['num_classes']
        self.epochs = self._parameters['epochs']
        self.batch_size = self._parameters['batch_size']
        self.val_batch_size = self._parameters['val_batch_size']


        self.save_model_folderpath = self._parameters['save_model_folderpath']




    def _read_config_file(self, config_filepath):
        """
        :param config_filepath:
        :return:
        """

        with open(config_filepath, 'r') as ymlfile:
            config_contents = yaml.safe_load(ymlfile)

        parameters = {}

        parameters['train_dataset_csv_filepath'] = config_contents['train_dataset_csv_filepath']
        parameters['train_dataset_root_filepath'] = config_contents['train_dataset_root_filepath']

        parameters['val_dataset_csv_filepath'] = config_contents['val_dataset_csv_filepath']
        parameters['val_dataset_root_filepath'] = config_contents['val_dataset_root_filepath']

        parameters['learning_rate'] = config_contents['learning_rate']
        parameters['num_classes'] = config_contents['num_classes']
        parameters['epochs'] = config_contents['epochs']
        parameters['batch_size'] = config_contents['batch_size']
        parameters['val_batch_size'] = config_contents['val_batch_size']

        parameters['save_model_folderpath'] = config_contents['save_model_folderpath']


        return parameters


class CnnVitTrainer:

    def __init__(self, config_reader: ConfigReader):
        """

        :param config_reader: a ConfigReader object containing configuration information from the yml file
        """

        # read information and parameters from config file
        self._config_reader = config_reader
        self._train_dataset_csv_filepath = self._config_reader.train_dataset_csv_filepath
        self._val_dataset_csv_filepath = self._config_reader.val_dataset_csv_filepath
        self._train_dataset_root_filepath = self._config_reader.train_dataset_root_filepath
        self._val_dataset_root_filepath = self._config_reader.val_dataset_root_filepath

        # initialise hyperparameters
        self.learning_rate = self._config_reader.learning_rate
        self.num_classes = self._config_reader.num_classes
        self.epochs = self._config_reader.epochs
        self.batch_size = self._config_reader.batch_size
        self.val_batch_size = self._config_reader.val_batch_size
        self.checkpoint_epochs = 10

        # set save path
        self.save_model_folderpath = self._config_reader.save_model_folderpath

        # initialise the dataloader
        classification_data_loader = ClassificationDataLoader()
        self._train_dataloader, self._train_dataloader_size = classification_data_loader.get_dataloader(root_folder_path=self._train_dataset_root_filepath,
                                                                                                        data_csv_filepath=self._train_dataset_csv_filepath,
                                                                                                        batch_size=self.batch_size,
                                                                                                        train=True)
        self._val_dataloader, self._val_dataloader_size = classification_data_loader.get_dataloader(root_folder_path=self._val_dataset_root_filepath,
                                                                                                    data_csv_filepath=self._val_dataset_csv_filepath,
                                                                                                    batch_size=self.val_batch_size,
                                                                                                    train=False)

        # initialise the hybrid CNN-ViT model
        self.model = HybridCnnVit(no_classes=self.num_classes,
                                  load_pretrained=True)



    def train(self):
        """
        Train the Hybrid CNN-ViT model from scratch (using pretrained ViT and Resnet18 weights from PyTorch).
        :return:
        """

        # Define the model and training options
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        print("Using device: ", device, f"({torch.cuda.get_device_name(device)})" if torch.cuda.is_available() else "")
        model = self.model.to(device)
        best_model_wts = copy.deepcopy(model.state_dict())


        # Training loop
        optimizer = Adam(model.parameters(), lr=self.learning_rate)
        criterion = CrossEntropyLoss()
        best_val_loss = None
        for epoch in trange(self.epochs, desc="Training"):

            # Training phase
            model.train()
            train_loss = 0.0

            for batch in tqdm(self._train_dataloader, desc=f"Epoch {epoch + 1} in training", leave=False):
                x, y = batch
                x_gpu, y_gpu = x.to(device), y.to(device)
                y_hat = model(x_gpu)
                loss = criterion(y_hat, y_gpu)

                train_loss += loss.detach().cpu().item() / self._train_dataloader_size

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

            print(f"Epoch {epoch + 1}/{self.epochs} loss: {train_loss:.2f}")

            # Validation phase
            model.eval()
            val_loss = 0.0

            for batch in tqdm(self._val_dataloader, desc=f"Epoch {epoch + 1} in training", leave=False):
                x, y = batch
                x_gpu, y_gpu = x.to(device), y.to(device)
                y_hat = model(x_gpu)
                loss = criterion(y_hat, y_gpu)

                val_loss += loss.detach().cpu().item() / self._val_dataloader_size

                if best_val_loss is None:
                    best_val_loss = val_loss
                elif val_loss < best_val_loss:
                    best_val_loss = val_loss
                    best_model_wts = copy.deepcopy(model.state_dict())

            print(f"Epoch {epoch + 1}/{self.epochs} current val loss: {val_loss:.2f}, best val loss: {best_val_loss:.2f}")

            # save model checkpoint every couple of epochs
            if epoch % self.checkpoint_epochs == 0 and epoch > 1:
                torch.save(obj={"epoch": epoch,
                                "model_state_dict": model.state_dict(),
                                "optimizer_state_dict": optimizer.state_dict(),
                                "best_model_state_dict": best_model_wts,
                                "best_val_loss": best_val_loss},
                           f=os.path.join(self.save_model_folderpath,f"cnn_vit_training_model_ckp_{epoch}.pth"))

        model.load_state_dict(best_model_wts)
        return model




    def save_model(self, model):
        torch.save(model.state_dict(), os.path.join(self.save_model_folderpath,"best_model.pt"))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config",
                        help="",
                        action="store")



    args =parser.parse_args()
    if args.config is not None:

        config_filepath = args.config

        config_reader = ConfigReader(config_filepath=config_filepath)
        trainer = CnnVitTrainer(config_reader=config_reader)
        model = trainer.train()
        trainer.save_model(model)



    else:
        print('Incorrect arguments...')



if __name__== "__main__":
    main()